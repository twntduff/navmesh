#include "Movement.h"

Movement::Movement(){

}

Movement::~Movement(){

}

DirectX::XMFLOAT3 Movement::Seek(const float maxSpeed, const DirectX::XMFLOAT3 target, const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 velocity) {
	DirectX::XMVECTOR desVel = DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&target), DirectX::XMLoadFloat3(&position));
	desVel = DirectX::XMVectorMultiply(DirectX::XMVector3Normalize(desVel), DirectX::XMVectorSet(maxSpeed, maxSpeed, maxSpeed, maxSpeed));
	desVel = DirectX::XMVectorSubtract(desVel, DirectX::XMLoadFloat3(&velocity));

	DirectX::XMFLOAT3 retVel;
	DirectX::XMStoreFloat3(&retVel, desVel);

	return retVel;
}