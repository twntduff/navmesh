#ifndef _CAMERA_H_
#define _CAMERA_H_

//Includes
#include <DirectXMath.h>
#include <Windows.h>
#include "Input.h"
#include "eObject.h"

class Camera : public eObject
{
public:
	Input* mInput;
	DirectX::XMFLOAT3 mRotation;
	DirectX::XMFLOAT3 mUp, mRight, mForward;
	DirectX::XMFLOAT3 mLookAt;
	float mSpeed;
	float mSensitivity;

public:
	Camera();
	~Camera();

	void Frame(const float dt);
	bool Initialize(Input* input, const DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), const DirectX::XMFLOAT3 pitchYawRoll = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	void BuildViewMatrix();
	void Rotate(Input* dInput);
	void Transform(float dt, Input* dInput);
};

#endif