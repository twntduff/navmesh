#include "System.h"
#include <vld.h>


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pScmdline, int iCmdshow)
{
	System system;

	//// Create the system object.
	//system = new System;
	//if (!system){
	//	return 0;
	//}

	// Initialize and run the system object.
	if (system.Initialize()){
		system.Run();
	}

	// Shutdown and release the system object.
	system.Shutdown();
	//delete system;
	//system = 0;

	return 0;
}