#include "Renderer.h"
#include "eSceneComp.h"

Renderer::Renderer(){

}

Renderer::~Renderer(){

}

bool Renderer::Initialize(ID3D11Device* device) {
	mShader.reset(new ColorShader);

	if (!mShader->Initialize(device)) {
		return false;
	}

	return true;
}

void Renderer::RenderScene(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, Direct3D* d3d, const DirectX::XMFLOAT4X4 view) {
	d3d->mDeviceContext->OMSetRenderTargets(1, &d3d->mRenderTargetView.p, d3d->mDepthStencilView.p);
	d3d->mDeviceContext->RSSetViewports(1, &d3d->mViewport);
	
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator iter = objects->begin();

	while (iter != objects->end()) {
		auto sceneComp = dynamic_cast<eSceneComp*>(iter->second.get());

		if (sceneComp) {
			sceneComp->Render(d3d->mDeviceContext);
			mShader->Render(d3d->mDeviceContext, sceneComp->mMesh->mIndexCount, sceneComp->mTransform, view, d3d->mProjectionMatrix);
		}
		iter++;
	}
}