#ifndef _RENDERER_H
#define _RENDERER_H_

//Includes
#include <memory>
#include <unordered_map>
#include <stdio.h>
#include <stdarg.h>
#include "Direct3D.h"
#include "eObject.h"
#include "ColorShader.h"

class Renderer
{
public:
	std::unique_ptr<ColorShader> mShader;

public:
	Renderer();
	~Renderer();

	bool Initialize(ID3D11Device* device);
	void RenderScene(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, Direct3D* d3d, const DirectX::XMFLOAT4X4 view);
};

#endif