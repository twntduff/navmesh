#include "eSceneComp.h"

eSceneComp::eSceneComp()
: eObject() {

}

eSceneComp::~eSceneComp() {

}

void eSceneComp::Initialize(Mesh* const mesh, const DirectX::XMFLOAT3 scale,
	const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 pitchYawRoll) {
	eObject::Initialize(position, pitchYawRoll);

	mMesh = mesh;
	mScale = scale;
}

void eSceneComp::Frame(const float dt) {
	DirectX::XMStoreFloat4x4(&mTransform, DirectX::XMMatrixScaling(mScale.x, mScale.y, mScale.z) * DirectX::XMMatrixRotationQuaternion(XMLoadFloat4(&mOrientation)) * DirectX::XMMatrixTranslationFromVector(XMLoadFloat3(&mPosition)));
}

void eSceneComp::Render(ID3D11DeviceContext* deviceContext) {
	mMesh->RenderBuffers(deviceContext);
}