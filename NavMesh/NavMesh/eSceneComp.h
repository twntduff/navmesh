#ifndef _ESCENECOMP_H_
#define _ESCENECOMP_H_

//Includes
#include <memory>
#include "eObject.h"
#include "Mesh.h"

class eSceneComp : public eObject
{
public:
	Mesh* mMesh;
	DirectX::XMFLOAT3 mScale;

public:
	eSceneComp();
	~eSceneComp();

	void Initialize(Mesh* const mesh, const DirectX::XMFLOAT3 scale = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f),
					const DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), const DirectX::XMFLOAT3 pitchYawRoll = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	void Frame(const float dt);
	void Render(ID3D11DeviceContext* deviceContext);
};

#endif