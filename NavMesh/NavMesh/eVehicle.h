#ifndef _EVEHICLE_H_
#define _EVEHICLE_H_

//Includes
#include "eSceneComp.h"
#include <queue>

class eVehicle : public eSceneComp
{
public:
	std::deque<DirectX::XMFLOAT3> mPath;
	DirectX::XMFLOAT3 mVelocity;
	float mMaxVelocity;

public:
	eVehicle();
	~eVehicle();

	void Initialize(Mesh* const mesh, const DirectX::XMFLOAT3 scale = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f),
		const DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), const DirectX::XMFLOAT3 pitchYawRoll = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f),
		const float maxVel = 20.0f);
	void Frame(const float dt);
};

#endif