#ifndef _INPUT_H_
#define _INPUT_H_


// Pre-Processing Directives 
#define DIRECTINPUT_VERSION 0x0800

//Linking
#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

#include <dinput.h>	

enum KeyState {
		NoChange,
		Pressed,
		Held,
		Released
	};

class Input
{
private:
	IDirectInput8* mDInput;
	IDirectInputDevice8* mKeyboard;
	IDirectInputDevice8* mMouse;
	DIMOUSESTATE2 mMouseState;
	char mKeyBuffer[256];
	char mCurrKeyBuffer[256];
	char mLastKeyBuffer[256];
	POINT mMousePosition;

public:
	Input();
	~Input();

	bool Initialize(HINSTANCE hInst, HWND hWnd);
	void Shutdown();
	void CheckDevice(HWND hWnd);
	bool KeyPressed(char key) { return (mKeyBuffer[key] & 0x80) != 0; };
	bool MousePressed(int button) { return (mMouseState.rgbButtons[button] & 0x80) != 0; };

	KeyState GetKeyState(int key);
	bool IsKeyPressed(int key);
	bool IsKeyHeld(int key);
	bool IsKeyReleased(int key);

	POINT GetMousePos();
	const float GetMouseDx() const;
	const float GetMouseDy() const;
	const float GetMouseDz() const;
};

#endif