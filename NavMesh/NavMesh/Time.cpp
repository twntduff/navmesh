#include "Time.h"

Time::Time(){
}

Time::~Time(){
}

const float Time::GetDeltaTime() const {
	return mElapsedTime;
}

const float Time::GetTime() const {
	return mTotalTime;
}

bool Time::Initialize(){	
	LARGE_INTEGER i;

	//get frequency from counter
	//does not change while system is running
	//only do this once
	if (!QueryPerformanceFrequency(&i))
		return false;

	//gives the number of ticks per second
	mFrequencySecs = 1.0f / (FLOAT)(i.QuadPart);

	//gets the current value of the counter
	QueryPerformanceCounter(&i);
	mStart = i.QuadPart;
	mTotalTime = 0.0f;
	mElapsedTime = 0.0f;

	return true;
}

void Time::Update(){
	LARGE_INTEGER i;

	QueryPerformanceCounter(&i);
	mElapsedTime = (FLOAT)(i.QuadPart - mStart) * mFrequencySecs;

	mStart = i.QuadPart;
	mTotalTime += mElapsedTime;
}