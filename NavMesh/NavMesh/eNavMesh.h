#ifndef _ENAVMESH_H_
#define _ENAVMESH_H_

//Includes
#include "eSceneComp.h"
#include "Mesh.h"
#include <deque>

#define COMPARE(a, b) a == b ? true : false

class eNavMesh : public eSceneComp
{
public:
	struct nmTriangle {
		float G;
		float H;
		DirectX::XMFLOAT3 mCentroid;
		DirectX::XMFLOAT3 mVerts[3];
		std::vector<nmTriangle*> mNextArr;
		nmTriangle* mParent;

		float GetF() {
			//float g = DirectX::XMVector3Length(DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&mCentroid), DirectX::XMLoadFloat3(&nextCentroid))).m128_f32[0];
			//float h = DirectX::XMVector3Length(DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&nextCentroid), DirectX::XMLoadFloat3(&goal))).m128_f32[0];
			return G + H;
		}
	};

public:
	std::vector<std::unique_ptr<nmTriangle>> mTriangles;

public:
	eNavMesh();
	~eNavMesh();

	void Initialize(Mesh *mesh, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 pitchYawRoll);
	void CalculateHValues(DirectX::XMFLOAT3 goal);
	eNavMesh::nmTriangle* FindContainingTriangle(DirectX::XMFLOAT3 point);
	std::deque<DirectX::XMFLOAT3> FindPath(DirectX::XMFLOAT3 start, DirectX::XMFLOAT3 goal);
};

#endif