#include "eVehicle.h"
#include "Movement.h"

eVehicle::eVehicle(){

}

eVehicle::~eVehicle(){

}

void eVehicle::Initialize(Mesh* const mesh, const DirectX::XMFLOAT3 scale, const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 pitchYawRoll, const float maxVel) {
	eSceneComp::Initialize(mesh, scale, position, pitchYawRoll);

	mVelocity = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
	mMaxVelocity = maxVel;
}

void eVehicle::Frame(const float dt) {
	eSceneComp::Frame(dt);

	if (!mPath.empty()) {
		mVelocity = Movement::Seek(mMaxVelocity, mPath[0], mPosition, mVelocity);

		if (DirectX::XMVector3Length(DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&mPosition), DirectX::XMLoadFloat3(&mPath[0]))).m128_f32[0] <= 0.5f) {
			mVelocity = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f);
			mPath.pop_front();
		}
	}

	DirectX::XMStoreFloat3(&mPosition, DirectX::XMVectorAdd(DirectX::XMLoadFloat3(&mPosition), DirectX::XMVectorMultiply(DirectX::XMLoadFloat3(&mVelocity), DirectX::XMVectorSet(dt, dt, dt, dt))));
}