#include "CollisionDetector.h"

CollisionDetector::CollisionDetector(){

}

CollisionDetector::~CollisionDetector(){

}

bool CollisionDetector::RayVsNavMeshIntersect(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, DirectX::XMFLOAT3 rayOrigin, DirectX::XMFLOAT3 rayDir, DirectX::XMFLOAT3 &interPoint) {
	eNavMesh* navMesh = NULL;

	for (unsigned int i = 0; i < objects->size(); i++) {
		navMesh = dynamic_cast<eNavMesh*>((*objects)[i].get());

		if (navMesh) {
			break;
		}
	}

	if (navMesh) {
		float t, u, v;

		for (unsigned int i = 0; i < navMesh->mTriangles.size(); i++) {
			DirectX::XMVECTOR edge1 = DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&navMesh->mTriangles[i]->mVerts[1]), DirectX::XMLoadFloat3(&navMesh->mTriangles[i]->mVerts[0]));
			DirectX::XMVECTOR edge2 = DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&navMesh->mTriangles[i]->mVerts[2]), DirectX::XMLoadFloat3(&navMesh->mTriangles[i]->mVerts[0]));

			DirectX::XMVECTOR pVec = DirectX::XMVector3Cross(DirectX::XMLoadFloat3(&rayDir), edge2);
			DirectX::XMVECTOR tVec = DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&rayOrigin), DirectX::XMLoadFloat3(&navMesh->mTriangles[i]->mVerts[0]));
			DirectX::XMVECTOR qVec = DirectX::XMVector3Cross(tVec, edge1);

			float determinant = DirectX::XMVector3Dot(edge1, pVec).m128_f32[0];
			float invDet = 1.0f / determinant;

			if (determinant > 0.000001) {
				u = DirectX::XMVector3Dot(tVec, pVec).m128_f32[0];

				if (u < 0.0f || u > determinant) {
					continue;
				}

				v = DirectX::XMVector3Dot(DirectX::XMLoadFloat3(&rayDir), qVec).m128_f32[0];

				if (v < 0.0f || u + v > determinant) {
					continue;
				}
			}
			else if (determinant < -0.000001) {
				u = DirectX::XMVector3Dot(tVec, pVec).m128_f32[0];

				if (u > 0.0f || u < determinant) {
					continue;
				}

				v = DirectX::XMVector3Dot(DirectX::XMLoadFloat3(&rayDir), qVec).m128_f32[0];

				if (v < 0.0f || u + v > determinant) {
					continue;
				}
			}
			else {
				continue;
			}

			t = DirectX::XMVector3Dot(edge2, qVec).m128_f32[0] * invDet;

			DirectX::XMStoreFloat3(&interPoint, DirectX::XMVectorAdd(DirectX::XMLoadFloat3(&rayOrigin), DirectX::XMVectorMultiply(DirectX::XMLoadFloat3(&rayDir), DirectX::XMVectorSet(t, t, t, 0.0f))));

			return true;
		}
	}

	return false;
}