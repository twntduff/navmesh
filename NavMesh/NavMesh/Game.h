#pragma once

//Includes
#include <memory>
#include "Direct3D.h"
#include "Level.h"

class Game
{
public:
	std::unique_ptr<Direct3D> mD3D;
	std::unique_ptr<Level> mCurrentLevel;

public:
	Game();
	~Game();

	bool Initialize(int screenWidth, int screenHeight, HWND hwnd);
	void Frame(float dt);
	void Render();

	//DEBUG
	void LoadTestLevelOne(Input* input);
	void LoadTestLevelTwo(Input* input);
	void LoadTestLevelThree(Input* input);
};

