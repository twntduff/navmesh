#ifndef _COLLISIONDETECTOR_H_
#define _COLLISIONDETECTOR_H_

//Includes
#include <d3d11.h>
#include <DirectXMath.h>
#include "ObjManager.h"

class CollisionDetector
{
public:
	CollisionDetector();
	~CollisionDetector();

	static bool RayVsNavMeshIntersect(std::unordered_map<unsigned int, std::unique_ptr<eObject>>* objects, DirectX::XMFLOAT3 rayOrigin, DirectX::XMFLOAT3 rayDir, DirectX::XMFLOAT3 &interPoint);
};

#endif