#ifndef _NAVMESH_H_
#define _NAVMESH_H_

//Includes
#include "Mesh.h"

class NavMesh : public Mesh
{
public:
	std::vector<VertexPC> mVertices;
	std::vector<UINT> mIndices;

public:
	NavMesh();
	~NavMesh();

	bool InitializeBuffers(ID3D11Device* device, std::vector<VertexPC>* vertices, std::vector<UINT>* indices);
};

#endif