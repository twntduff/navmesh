#include "Camera.h"

using namespace DirectX;

Camera::Camera(){
	DirectX::XMStoreFloat3(&mPosition, DirectX::XMVectorZero());
	DirectX::XMStoreFloat3(&mRotation, DirectX::XMVectorZero());
	DirectX::XMStoreFloat3(&mLookAt, DirectX::XMVectorZero());

	mUp = DirectX::XMFLOAT3(0.0f, 1.0f, 0.0f);
	mRight = DirectX::XMFLOAT3(1.0f, 0.0f, 0.0f);
	mForward = DirectX::XMFLOAT3(0.0f, 0.0f, 1.0f);

	mSpeed = 20.0f;
	mSensitivity = 0.001f;
}

Camera::~Camera(){

}

void Camera::BuildViewMatrix()
{
	DirectX::XMStoreFloat4x4(&mTransform, DirectX::XMMatrixLookAtLH(DirectX::XMLoadFloat3(&mPosition), DirectX::XMLoadFloat3(&mLookAt), DirectX::XMLoadFloat3(&mUp)));
}

bool Camera::Initialize(Input* input, const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 pitchYawRoll)
{
	eObject::Initialize(position, pitchYawRoll);

	mInput = input;
	mForward = GetAxis(2);
	DirectX::XMStoreFloat3(&mLookAt, DirectX::XMLoadFloat3(&mForward) * 10.0f);
	
	return true;
}

void Camera::Rotate(Input* input)
{
	if (input->MousePressed(1)){
		float pitch = input->GetMouseDy();
		float yAngle = input->GetMouseDx();

		pitch *= mSensitivity;
		yAngle *= mSensitivity;

		DirectX::XMMATRIX rotAxis;
		DirectX::XMVECTOR right = XMLoadFloat3(&mRight);
		rotAxis = DirectX::XMMatrixRotationAxis(right, pitch);
		DirectX::XMVECTOR forward = DirectX::XMLoadFloat3(&mForward);
		forward = DirectX::XMVector3TransformCoord(forward, rotAxis);
		DirectX::XMVECTOR up = DirectX::XMLoadFloat3(&mUp);
		up = DirectX::XMVector3TransformCoord(up, rotAxis);

		DirectX::XMMATRIX rotYAxis;
		rotYAxis = DirectX::XMMatrixRotationY(yAngle);
		right = DirectX::XMVector3TransformCoord(right, rotYAxis);
		up = DirectX::XMVector3TransformCoord(up, rotYAxis);
		forward = DirectX::XMVector3TransformCoord(forward, rotYAxis);
		
		DirectX::XMStoreFloat3(&mForward, forward);
		DirectX::XMStoreFloat3(&mUp, up);
		DirectX::XMStoreFloat3(&mRight, right);
	}
}

void Camera::Transform(float dt, Input* dInput) {
	float speedSet = mSpeed;
	if (dInput->IsKeyHeld(DIK_LSHIFT)){
		speedSet *= 5;
	}

	DirectX::XMVECTOR direction = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);
	DirectX::XMVECTOR right = DirectX::XMLoadFloat3(&mRight);
	DirectX::XMVECTOR forward = DirectX::XMLoadFloat3(&mForward);

	if (dInput->IsKeyHeld(DIK_W)){
		direction =+ forward;
	}

	if (dInput->IsKeyHeld(DIK_S)){
		direction -= forward;
	}

	if (dInput->IsKeyHeld(DIK_D)){
		direction += right;
	}

	if (dInput->IsKeyHeld(DIK_A)){
		direction -= right;
	}

	if (dInput->IsKeyHeld(DIK_Q)){
		direction.m128_f32[1] += 1;
	}

	if (dInput->IsKeyHeld(DIK_E)){
		direction.m128_f32[1] -= 1;
	}

	direction = DirectX::XMVector3Normalize(direction);
	float deltaSpeedSet = speedSet * dt;

	DirectX::XMStoreFloat3(&mPosition, DirectX::XMLoadFloat3(&mPosition) + direction * deltaSpeedSet);
	DirectX::XMStoreFloat3(&mLookAt, DirectX::XMLoadFloat3(&mLookAt) + direction * deltaSpeedSet);
}

void Camera::Frame(const float dt)
{
	Rotate(mInput);
	Transform(dt, mInput);
	
	DirectX::XMVECTOR forward = DirectX::XMLoadFloat3(&mForward);
	DirectX::XMVECTOR right = DirectX::XMLoadFloat3(&mRight);
	DirectX::XMVECTOR up = DirectX::XMLoadFloat3(&mUp);
	forward = DirectX::XMVector3Normalize(forward);
	right = DirectX::XMVector3Normalize(right);

	up = DirectX::XMVector3Cross(forward, right);
	up = DirectX::XMVector3Normalize(up);

	DirectX::XMStoreFloat3(&mForward, forward);
	DirectX::XMStoreFloat3(&mRight, right);
	DirectX::XMStoreFloat3(&mUp, up);

	DirectX::XMStoreFloat3(&mLookAt, DirectX::XMVectorMultiplyAdd(DirectX::XMVectorSet(10.0f, 10.0f, 10.0f, 1.0f), forward, DirectX::XMLoadFloat3(&mPosition)));
	
	BuildViewMatrix();
}