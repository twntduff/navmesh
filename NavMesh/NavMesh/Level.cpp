#include "Level.h"

Level::Level(){

}

Level::~Level(){

}

bool Level::Initialize(ID3D11Device* device, Input* input, DirectX::XMFLOAT3 camPosition, DirectX::XMFLOAT3 camPitchYawRoll) {
	mMeshManager.reset(new MeshManager);

	mObjectManager.reset(new ObjManager);
	if (!mObjectManager->Initialize(device)) {
		return false;
	}

	// Create the camera object.
	mCamera = new Camera;
	if (!mCamera->Initialize(input, camPosition, camPitchYawRoll)) {
		return false;
	}

	mObjectManager->AddObject(mCamera);

	return true;
}

void Level::Frame(const float dt) {
	mObjectManager->UpdateObjects(dt);
}

void Level::Render(Direct3D* d3d) {
	mObjectManager->Render(d3d, mCamera->mTransform);
}

bool Level::Exit() {
	mMeshManager.release();

	return true;
}