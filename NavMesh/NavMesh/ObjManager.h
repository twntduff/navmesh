#ifndef _OBJMANAGER_H_
#define _OBJMANAGER_H_

//Includes
#include "Renderer.h"
#include "Mesh.h"
#include "eNavMesh.h"

//Globals
const bool FULL_SCREEN = false;
const bool VSYNC_ENABLED = false;
const float SCREEN_DEPTH = 2000.0f;
const float SCREEN_NEAR = 0.01f;
const unsigned int SCREEN_WIDTH = 900;
const unsigned int SCREEN_HEIGHT = 600;

class ObjManager
{
private:
	std::unique_ptr<Renderer> mRenderer;

public:
	std::unordered_map<unsigned int, std::unique_ptr<eObject>> mObjects;

public:
	ObjManager();
	~ObjManager();

	bool Initialize(ID3D11Device* device);
	unsigned int GetNextValidID();
	eObject* AddObject(eObject* const object);
	eObject* AddSceneComp(Mesh* const mesh, const DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f),
		const DirectX::XMFLOAT3 scale = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f), const DirectX::XMFLOAT3 pitchYawRoll = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	eObject* AddNavMesh(Mesh* const mesh, const DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f),
		const DirectX::XMFLOAT3 scale = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f), const DirectX::XMFLOAT3 pitchYawRoll = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	eObject* AddVehicle(Mesh* const mesh, const DirectX::XMFLOAT3 position = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f),
		const DirectX::XMFLOAT3 scale = DirectX::XMFLOAT3(1.0f, 1.0f, 1.0f), const DirectX::XMFLOAT3 pitchYawRoll = DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f), const float maxVelocity = 50.0f);
	bool DeleteAllObjects();
	bool DeleteObject(unsigned int id);
	void UpdateObjects(const float dt);
	void Render(Direct3D* d3d, const DirectX::XMFLOAT4X4 view);
	void ScreenPicked(POINT mousePos, DirectX::XMFLOAT3 rayOrigin, DirectX::XMFLOAT4X4 view, DirectX::XMFLOAT4X4 projection, unsigned int screenWidth, unsigned int screenHeight);
};

#endif