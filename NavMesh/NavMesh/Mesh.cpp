#include "Mesh.h"

Mesh::Mesh(){

}

Mesh::~Mesh(){

}

bool Mesh::InitializeBuffers(ID3D11Device* device, std::vector<VertexPC>* vertices, std::vector<UINT>* indices) {
	HRESULT hresult;
	D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData, indexData;
	VertexPC* d3dVertices;
	unsigned long* d3dIndices = nullptr;

	mVertexCount = (unsigned int)vertices->size();
	mIndexCount = (unsigned int)indices->size();

	d3dVertices = new VertexPC[mVertexCount];
	if (!d3dVertices) {
		return false;
	}

	d3dIndices = new unsigned long[mIndexCount];
	if (!d3dIndices) {
		return false;
	}

	for (unsigned int i = 0; i < vertices->size(); i++) {
		d3dVertices[i] = vertices->at(i);
	}

	for (unsigned int i = 0; i < indices->size(); i++) {
		d3dIndices[i] = indices->at(i);
	}

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(VertexPC) * mVertexCount;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = d3dVertices;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	hresult = device->CreateBuffer(&vertexBufferDesc, &vertexData, &mVertexBuffer);
	if (FAILED(hresult)) {
		return false;
	}

	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(unsigned long) * mIndexCount;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = d3dIndices;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	hresult = device->CreateBuffer(&indexBufferDesc, &indexData, &mIndexBuffer);
	if (FAILED(hresult)) {
		return false;
	}

	// Release the arrays now that the vertex and index buffers have been created and loaded.
	delete[] d3dVertices;
	d3dVertices = 0;

	delete[] d3dIndices;
	d3dIndices = 0;

	return true;
}

void Mesh::RenderBuffers(ID3D11DeviceContext* deviceContext) {
	unsigned int stride;
	unsigned int offset;

	// Set vertex buffer stride and offset.
	stride = sizeof(VertexPC);
	offset = 0;

	// Set the vertex buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetVertexBuffers(0, 1, &mVertexBuffer.p, &stride, &offset);

	// Set the index buffer to active in the input assembler so it can be rendered.
	deviceContext->IASetIndexBuffer(mIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	// Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
	deviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}