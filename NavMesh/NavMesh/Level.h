#ifndef _LEVEL_H_
#define _LEVEL_H_

//Includes
#include "MeshManager.h"
#include "Camera.h"
#include "ObjManager.h"
#include "Direct3D.h"

class Level
{
public:
	std::unique_ptr<ObjManager> mObjectManager;
	std::unique_ptr<MeshManager> mMeshManager;
	Camera* mCamera;

public:
	Level();
	~Level();

	bool Initialize(ID3D11Device* device, Input* input, DirectX::XMFLOAT3 camPosition, DirectX::XMFLOAT3 camPitchYawRoll);
	void Frame(const float dt);
	void Render(Direct3D* d3d);
	bool Exit();
};

#endif