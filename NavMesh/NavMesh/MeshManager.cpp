#include "MeshManager.h"
#include "NavMesh.h"

MeshManager::MeshManager(){

}

MeshManager::~MeshManager(){

}

bool MeshManager::LoadAndRegisterMeshFromOBJ(ID3D11Device* device, const std::string meshFile) {
	std::unique_ptr<Mesh> mesh(new Mesh);
	std::vector<DirectX::XMFLOAT3> positions;
	std::vector<DirectX::XMFLOAT4> colors;

	std::vector<VertexPC> vertices;
	std::vector<UINT> indices;

	char cmd[256] = { 0 };

	std::ifstream file(meshFile);

	if (!file) {
		return false;
	}

	while (true) {
		file >> cmd;
		if (!file) {
			break;
		}

		if (strcmp(cmd, "#") == 0) {

		}
		else if (strcmp(cmd, "v") == 0) {
			float x, y, z;
			file >> x >> y >> z;

			positions.push_back(DirectX::XMFLOAT3(x, y, z));
		}
		else if (strcmp(cmd, "c") == 0) {
			float x, y, z, w;
			file >> x >> y >> z >> w;

			colors.push_back(DirectX::XMFLOAT4(x, y, z, w));
		}
		else if (strcmp(cmd, "f") == 0) {
			for (unsigned int i = 0; i < 3; i++) {
				bool found = false;
				UINT value;
				VertexPC vertex;

				file >> value;
				vertex.position = positions[value - 1];
				file.ignore();

				file >> value;
				vertex.color = colors[value - 1];
				file.ignore();

				for (unsigned int j = 0; j < vertices.size(); j++) {
					if (vertex.position.x == vertices[j].position.x && vertex.position.y == vertices[j].position.y && vertex.position.z == vertices[j].position.z &&
						vertex.color.x == vertices[j].color.x && vertex.color.y == vertices[j].color.y && vertex.color.z == vertices[j].color.z && vertex.color.w == vertices[j].color.w) {
						indices.push_back(j);
						found = true;
					}
				}

				if (!found) {
					vertices.push_back(vertex);
					indices.push_back((unsigned int)vertices.size() - 1);
				}
			}
		}
	}

	file.close();

	if (!mesh->InitializeBuffers(device, &vertices, &indices)) {
		return false;
	}

	std::string key = meshFile.substr(7, meshFile.find(".", 0) - 7);

	mesh.swap(mMeshes[key]);

	return true;
}

bool MeshManager::LoadAndRegisterNavMeshFromOBJ(ID3D11Device* device, const std::string meshFile) {
	std::unique_ptr<Mesh> mesh(new NavMesh);
	std::vector<DirectX::XMFLOAT3> positions;
	std::vector<DirectX::XMFLOAT4> colors;

	std::vector<VertexPC> vertices;
	std::vector<UINT> indices;

	char cmd[256] = { 0 };

	std::ifstream file(meshFile);

	if (!file) {
		return false;
	}

	while (true) {
		file >> cmd;
		if (!file) {
			break;
		}

		if (strcmp(cmd, "#") == 0) {

		}
		else if (strcmp(cmd, "v") == 0) {
			float x, y, z;
			file >> x >> y >> z;

			positions.push_back(DirectX::XMFLOAT3(x, y, z));
		}
		else if (strcmp(cmd, "c") == 0) {
			float x, y, z, w;
			file >> x >> y >> z >> w;

			colors.push_back(DirectX::XMFLOAT4(x, y, z, w));
		}
		else if (strcmp(cmd, "f") == 0) {
			for (unsigned int i = 0; i < 3; i++) {
				bool found = false;
				UINT value;
				VertexPC vertex;

				file >> value;
				vertex.position = positions[value - 1];
				file.ignore();

				file >> value;
				vertex.color = colors[value - 1];
				file.ignore();

				for (unsigned int j = 0; j < vertices.size(); j++) {
					if (vertex.position.x == vertices[j].position.x && vertex.position.y == vertices[j].position.y && vertex.position.z == vertices[j].position.z &&
						vertex.color.x == vertices[j].color.x && vertex.color.y == vertices[j].color.y && vertex.color.z == vertices[j].color.z && vertex.color.w == vertices[j].color.w) {
						indices.push_back(j);
						found = true;
					}
				}

				if (!found) {
					vertices.push_back(vertex);

					unsigned int ind = (unsigned int)vertices.size();
					ind--;
					indices.push_back(ind);
				}
			}
		}
	}

	file.close();

	if (!mesh->InitializeBuffers(device, &vertices, &indices)) {
		return false;
	}

	std::string key = meshFile.substr(7, meshFile.find(".", 0) - 7);

	mesh.swap(mMeshes[key]);

	return true;
}

Mesh* MeshManager::RetrieveMesh(const std::string key) {
	std::unordered_map<std::string, std::unique_ptr<Mesh>>::const_iterator found = mMeshes.find(key);

	if (found->second) {
		return found->second.get();
	}

	return nullptr;
}