#ifndef _MESH_H_
#define _MESH_H_

//Includes
#include <d3d11.h>
#include <atlbase.h>
#include <DirectXMath.h>
#include <vector>

struct VertexPC {
	DirectX::XMFLOAT3 position;
	DirectX::XMFLOAT4 color;
};

class Mesh
{
public:
	ATL::CComPtr<ID3D11Buffer> mIndexBuffer;
	ATL::CComPtr<ID3D11Buffer> mVertexBuffer;
	unsigned int mIndexCount;
	unsigned int mVertexCount;

public:
	Mesh();
	virtual ~Mesh();

	virtual bool InitializeBuffers(ID3D11Device* device, std::vector<VertexPC>* vertices, std::vector<UINT>* indices);
	void RenderBuffers(ID3D11DeviceContext* deviceContext);
};

#endif