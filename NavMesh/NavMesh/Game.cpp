#include "Game.h"
#include <dxgidebug.h>

Game::Game() {
	mD3D = nullptr;
	mCurrentLevel = nullptr;
}

Game::~Game() {
	//Shutdown();
}

bool Game::Initialize(int screenWidth, int screenHeight, HWND hwnd) {
	// Create the Direct3D object.
	mD3D.reset(new Direct3D);
	
	if (!mD3D->Initialize(screenWidth, screenHeight, VSYNC_ENABLED, hwnd, FULL_SCREEN, SCREEN_DEPTH, SCREEN_NEAR)) {
		MessageBox(hwnd, L"Could not initialize Direct3D", L"Error", MB_OK);
		return false;
	}

	return true;
}

void Game::Frame(float dt) {
	//Update level objects
	if (mCurrentLevel) {
		mCurrentLevel->Frame(dt);
	}

	// Render the scene.
	Render();
}

void Game::Render() {
	if (mCurrentLevel) {
		// Clear the buffers to begin the scene.
		mD3D->BeginScene(0.5f, 0.5f, 0.5f, 1.0f);

		mCurrentLevel->Render(mD3D.get());

		// Present the rendered scene to the screen.
		mD3D->EndScene();
	}
}

void Game::LoadTestLevelOne(Input* input) {
	mCurrentLevel.reset(new Level);
	if (!mCurrentLevel->Initialize(mD3D->mDevice, input, DirectX::XMFLOAT3(0.0f, 140.0f, -140.0f), DirectX::XMFLOAT3(DirectX::XMConvertToRadians(-45.0f), 0.0f, 0.0f))) {
		return;
	}

	mCurrentLevel->mMeshManager->LoadAndRegisterNavMeshFromOBJ(mD3D->mDevice, "Meshes\\NavMesh.obj");
	mCurrentLevel->mMeshManager->LoadAndRegisterMeshFromOBJ(mD3D->mDevice, "Meshes\\Box.obj");

	mCurrentLevel->mObjectManager->AddNavMesh(mCurrentLevel->mMeshManager->RetrieveMesh("NavMesh"), DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCurrentLevel->mObjectManager->AddVehicle(mCurrentLevel->mMeshManager->RetrieveMesh("Box"), DirectX::XMFLOAT3(-45.0f, 0.0f, 0.0f));
}

void Game::LoadTestLevelTwo(Input* input) {
	mCurrentLevel.reset(new Level);
	if (!mCurrentLevel->Initialize(mD3D->mDevice, input, DirectX::XMFLOAT3(0.0f, 100.0f, -100.0f), DirectX::XMFLOAT3(DirectX::XMConvertToRadians(-45.0f), 0.0f, 0.0f))) {
		return;
	}

	mCurrentLevel->mMeshManager->LoadAndRegisterNavMeshFromOBJ(mD3D->mDevice, "Meshes\\NavMesh02.obj");
	mCurrentLevel->mMeshManager->LoadAndRegisterMeshFromOBJ(mD3D->mDevice, "Meshes\\Box.obj");

	mCurrentLevel->mObjectManager->AddNavMesh(mCurrentLevel->mMeshManager->RetrieveMesh("NavMesh02"), DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCurrentLevel->mObjectManager->AddVehicle(mCurrentLevel->mMeshManager->RetrieveMesh("Box"), DirectX::XMFLOAT3(-20.0f, 0.0f, -25.0f));
}

void Game::LoadTestLevelThree(Input* input) {
	mCurrentLevel.reset(new Level);
	if (!mCurrentLevel->Initialize(mD3D->mDevice, input, DirectX::XMFLOAT3(0.0f, 60.0f, -100.0f), DirectX::XMFLOAT3(DirectX::XMConvertToRadians(-35.0f), 0.0f, 0.0f))) {
		return;
	}

	mCurrentLevel->mMeshManager->LoadAndRegisterNavMeshFromOBJ(mD3D->mDevice, "Meshes\\NavMesh03.obj");
	mCurrentLevel->mMeshManager->LoadAndRegisterMeshFromOBJ(mD3D->mDevice, "Meshes\\Box.obj");

	mCurrentLevel->mObjectManager->AddNavMesh(mCurrentLevel->mMeshManager->RetrieveMesh("NavMesh03"), DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f));
	mCurrentLevel->mObjectManager->AddVehicle(mCurrentLevel->mMeshManager->RetrieveMesh("Box"), DirectX::XMFLOAT3(-25.0f, 0.0f, 25.0f));
}