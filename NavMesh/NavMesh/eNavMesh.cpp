#include "eNavMesh.h"
#include "NavMesh.h"

eNavMesh::eNavMesh()
	: eSceneComp() {

}

eNavMesh::~eNavMesh() {
	
}

void eNavMesh::Initialize(Mesh *mesh, DirectX::XMFLOAT3 scale, DirectX::XMFLOAT3 position, DirectX::XMFLOAT3 pitchYawRoll) {
	eSceneComp::Initialize(mesh, scale, position, pitchYawRoll);

	auto navMesh = dynamic_cast<NavMesh*>(mesh);

	if (navMesh) {
		mTriangles.resize(navMesh->mIndexCount / 3);

		unsigned int crIter = 0;

		//Create trainagles
		for (unsigned int i = 0; i < mTriangles.size(); i++) {
			//Create triangle object
			std::unique_ptr<nmTriangle> triangle;
			triangle.reset(new nmTriangle);
			triangle.swap(mTriangles[i]);

			for (unsigned int k = 0; k < 3; k++) {
				//Assign verts
				mTriangles[i]->mVerts[k] = navMesh->mVertices[navMesh->mIndices[crIter + k]].position;
			}

			crIter += 3;

			//Caluculate centroid
			DirectX::XMFLOAT3 vert0 = mTriangles[i]->mVerts[0];
			DirectX::XMFLOAT3 vert1 = mTriangles[i]->mVerts[1];
			DirectX::XMFLOAT3 vert2 = mTriangles[i]->mVerts[2];

			mTriangles[i]->mCentroid = DirectX::XMFLOAT3((vert0.x + vert1.x + vert2.x) / 3,
				(vert0.y + vert1.y + vert2.y) / 3,
				(vert0.z + vert1.z + vert2.z) / 3);
		}

		//Connect triangles
		for (unsigned int i = 0; i < mTriangles.size(); i++) {
			for (unsigned int j = i + 1; j < mTriangles.size(); j++) {
				unsigned int coIter = 0;

				for (unsigned int k = 0; k < 3; k++) {
					for (unsigned int m = 0; m < 3; m++) {
						if (mTriangles[i]->mVerts[k].x == mTriangles[j]->mVerts[m].x &&
							mTriangles[i]->mVerts[k].y == mTriangles[j]->mVerts[m].y &&
							mTriangles[i]->mVerts[k].z == mTriangles[j]->mVerts[m].z) {
							coIter++;
						}
					}
				}

				if (coIter > 1) {
					mTriangles[i]->mNextArr.push_back(mTriangles[j].get());
					mTriangles[j]->mNextArr.push_back(mTriangles[i].get());
				}
			}
		}
	}
}

void eNavMesh::CalculateHValues(DirectX::XMFLOAT3 goal) {
	for (unsigned int i = 0; i < mTriangles.size(); i++) {
		mTriangles[i]->H = DirectX::XMVector3Length(DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&mTriangles[i]->mCentroid), DirectX::XMLoadFloat3(&goal))).m128_f32[0];
	}
}

bool SameSide(DirectX::XMFLOAT3 vertA, DirectX::XMFLOAT3 vertB, DirectX::XMFLOAT3 pointA, DirectX::XMFLOAT3 pointB) {
	using namespace DirectX;

	XMVECTOR cp1 = XMVector3Cross(XMLoadFloat3(&vertB) - XMLoadFloat3(&vertA), XMLoadFloat3(&pointA) - XMLoadFloat3(&vertA));
	XMVECTOR cp2 = XMVector3Cross(XMLoadFloat3(&vertB) - XMLoadFloat3(&vertA), XMLoadFloat3(&pointB) - XMLoadFloat3(&vertA));

	if (XMVector3Dot(cp1, cp2).m128_f32[0] >= 0.0f) {
		return true;
	}

	return false;
}

eNavMesh::nmTriangle* eNavMesh::FindContainingTriangle(DirectX::XMFLOAT3 point) {
	for (unsigned int i = 0; i < mTriangles.size(); i++) {
		if (SameSide(mTriangles[i]->mVerts[1], mTriangles[i]->mVerts[2], point, mTriangles[i]->mVerts[0]) &&
			SameSide(mTriangles[i]->mVerts[0], mTriangles[i]->mVerts[2], point, mTriangles[i]->mVerts[1]) &&
			SameSide(mTriangles[i]->mVerts[0], mTriangles[i]->mVerts[1], point, mTriangles[i]->mVerts[2])) {
			return mTriangles[i].get();
		}
	}

	return nullptr;
}

bool OnSegment(DirectX::XMFLOAT2 p, DirectX::XMFLOAT2 q, DirectX::XMFLOAT2 r) {
	if (q.x <= fmaxf(p.x, r.x) && q.x >= fminf(p.x, r.x) &&
		q.y <= fmaxf(p.y, r.y) && q.y >= fminf(p.y, r.y)) {
		return true;
	}

	return false;
}

unsigned int LineOrientation(DirectX::XMFLOAT2 p, DirectX::XMFLOAT2 q, DirectX::XMFLOAT2 r) {
	int retVal = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);

	if (retVal == 0) {
		return 0;
	}

	return (retVal > 0) ? 1 : 2;
}

bool DoLinesIntersect2D(DirectX::XMFLOAT2 s1, DirectX::XMFLOAT2 e1, DirectX::XMFLOAT2 s2, DirectX::XMFLOAT2 e2, DirectX::XMFLOAT2 &interPoint) {
	float d = (e2.y - s2.y) * (e1.x - s1.x) - (e2.x - s2.x) * (e1.y - s1.y);
	float a = (e2.x - s2.x) * (s1.y - s2.y) - (e2.y - s2.y) * (s1.x - s2.x);
	float b = (e1.x - s1.x) * (s1.y - s2.y) - (e1.y - s1.y) * (s1.x - s1.x);
	float u1 = a / d;
	//float u2 = b / d;

	if (d == 0.0f && a == 0.0f && b == 0.0f ||
		d == 0.0f ||
		u1 < 0.0f || u1 > 1.0f) {
		

		return false;
	}
	
	float x = ((s2.x - e2.x) * (s1.x * e1.y - s1.y * e1.x) - (s1.x - e1.x) * (s2.x * e2.y - s2.y * e2.x)) / d;
	float y = ((s2.y - e2.y) * (s1.x * e1.y - s1.y * e1.x) - (s1.y - e1.y) * (s2.x * e2.y - s2.y * e2.x)) / d;

	interPoint.x = x;
	interPoint.y = y;

	return true;
}

bool FindY(DirectX::XMFLOAT3 p1, DirectX::XMFLOAT3 p2, float x, float z, float &y) {
	float t;

	if (p2.x - p1.x != 0.0f) {
		t = (x - p1.x) / (p2.x - p1.x);
	}
	else if (p2.z - p1.z != 0.0f) {
		t = (z - p1.z) / (p2.z - p1.z);
	}
	else {
		return false;
	}

	y = p1.y + (p2.y - p1.y) * t;

	return true;
}

std::deque<DirectX::XMFLOAT3> eNavMesh::FindPath(DirectX::XMFLOAT3 start, DirectX::XMFLOAT3 goal) {
	std::deque<DirectX::XMFLOAT3> path;
	////////
	//A-Star
	////////
	//Check if points are contained in Tris
	auto sTri = FindContainingTriangle(start);
	auto gTri = FindContainingTriangle(goal);

	//If either is outside of navMesh return empty path
	if (!sTri || !gTri) {
		return path;
	}
	//If Tris are the same, return goal position
	else if (sTri == gTri) {
		path.push_back(goal);

		return path;
	}

	//Calculate H values for nodes
	CalculateHValues(goal);
	std::vector<nmTriangle*> open;
	std::vector<nmTriangle*> closed;

	//Set initial G at 0
	sTri->G = 0.0f;
	sTri->mParent = NULL;
	open.push_back(sTri);
	auto currTri = (*open.begin());

	//While there are still nodes to check
	while (!open.empty()) {
		float lowestF = FLT_MAX;

		//Find node with lowest F value
		unsigned int iter = 0;
		for (unsigned int i = 0; i < open.size(); i++) {
			if (open[i]->GetF() < lowestF) {
				lowestF = open[i]->GetF();
				currTri = open[i];
				iter = i;
			}
		}

		//If current node is equal to the goal node
		//Break out of the while loop
		if (currTri == gTri) {
			break;
		}

		//Erase current node from open list
		//Place current node in closed list
		open.erase(open.begin() + iter);
		closed.push_back(currTri);

		//Loop through each diagonal node
		for (unsigned int i = 0; i < currTri->mNextArr.size(); i++) {
			bool isBetter = false;
			bool cFound = false;
			bool oFound = false;

			//Loop through closed list
			for (unsigned int j = 0; j < closed.size(); j++) {
				//Check if current node is in the closed list
				if (currTri->mNextArr[i] == closed[j]) {
					cFound = true;
				}
			}

			//If node is not already in the closed list
			if (!cFound) {
				//Calculate current node's G value
				float currG = currTri->G + DirectX::XMVector3Length(DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&currTri->mCentroid), DirectX::XMLoadFloat3(&currTri->mNextArr[i]->mCentroid))).m128_f32[0];

				//Loop through open list
				for (unsigned int j = 0; j < open.size(); j++) {
					//Check if current node is in the open list
					if (currTri->mNextArr[i] == open[j]) {
						oFound = true;

						//Check if current G value is less than previous G value
						if (currG <= open[j]->G) {
							isBetter = true;
						}
					}
				}

				//If node has not been checked before OR
				//If there is a copy of the current node in the open list with a lower F value
				//and  it is not already in the closed list OR
				//The current node was not found in the open list
				//Place new node into the open list
				if (!oFound || isBetter) {
					open.push_back(currTri->mNextArr[i]);
					currTri->mNextArr[i]->mParent = currTri;
					currTri->mNextArr[i]->G = currG;
				}
			}
		}
	}
	//////////////
	//Find portals
	/////////////
	//Determine the shared edges of the Tris, or portals, and determine which vert 
	//is left and which is right relative to the agent traversing the path
	std::deque<DirectX::XMFLOAT3> left;
	left.push_back(goal);
	std::deque<DirectX::XMFLOAT3> right;
	right.push_back(goal);
	std::vector<DirectX::XMFLOAT3> edge;
	DirectX::XMFLOAT3 other;

	//Loop through current nodes parents until we reached the start node
	while (currTri->mParent) {
		bool vertFound = false;
		//Find shared vertices
		//Search through parent verts and current node verts
		for (unsigned int i = 0; i < 3; i++) {
			for (unsigned int j = 0; j < 3; j++) {
				//If verts are shared then add them to the egde array
				if (currTri->mVerts[j].x == currTri->mParent->mVerts[i].x &&
					currTri->mVerts[j].y == currTri->mParent->mVerts[i].y &&
					currTri->mVerts[j].z == currTri->mParent->mVerts[i].z) {
					edge.push_back(currTri->mVerts[j]);
					vertFound = true;
				}
			}

			//If the current nodes vert does not match a vert from the parent nodes verts,
			//save the unique vert for left/right calculations
			if (!vertFound) {
				other = currTri->mParent->mVerts[i];
			}
			else {
				vertFound = false;
			}
		}

		//TODO: Calculate the polys normal so that the navmesh can be followed at extreme angles
		//Set the normal to (0, 1, 0) because we will not be using navmeshes that can be traversed at extreme angles
		DirectX::XMVECTOR normal = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
		
		//This method of normal calculation is dependent on vert order and may give false normal.
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		//DirectX::XMVector3Normalize(
			//DirectX::XMVector3Cross(
				//DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&edge[0]), DirectX::XMLoadFloat3(&other)),
				//DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&edge[1]), DirectX::XMLoadFloat3(&other))));
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

		DirectX::XMVECTOR direction = DirectX::XMVector3Normalize(
			DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&currTri->mParent->mCentroid), DirectX::XMLoadFloat3(&other)));

		//Calculate the determinant
		float side = DirectX::XMVector3Dot(DirectX::XMVector3Cross(DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&edge[0]), DirectX::XMLoadFloat3(&other)), direction), normal).m128_f32[0];

		//if side < 0, then edge[0] is on the left
		if (side > 0.0f) {
			left.push_front(edge[0]);
			right.push_front(edge[1]);
		}
		//If side > 0, then edge[0] is on the right
		else {
			right.push_front(edge[0]);
			left.push_front(edge[1]);
		}

		//Clear the edge array and set current node to current node's parent
		edge.clear();
		currTri = currTri->mParent;
	}

	/////////////////////////////////
	//Simple stupid funnel algorithm
	////////////////////////////////
	unsigned int leftIter = 0;
	unsigned int rightIter = 0;
	bool movedLeft = false;
	bool overlap = false;
	DirectX::XMFLOAT3 apex = start;
	float angle = FLT_MAX;
	std::vector<DirectX::XMFLOAT3> apexArr;
	apexArr.push_back(start);

	while (leftIter < left.size() && rightIter < right.size()) {
		float newAngle =	DirectX::XMVector3AngleBetweenVectors(
							DirectX::XMVector3Normalize(
							DirectX::XMVectorSubtract(DirectX::XMVectorSet(left[leftIter].x, 0.0f, left[leftIter].z, 0.0f), DirectX::XMVectorSet(apex.x, 0.0f, apex.z, 0.0f))), 
							DirectX::XMVector3Normalize(
							DirectX::XMVectorSubtract(DirectX::XMVectorSet(right[rightIter].x, 0.0f, right[rightIter].z, 0.0f), DirectX::XMVectorSet(apex.x, 0.0f, apex.z, 0.0f)))).m128_f32[0];
	
		//Calculate the determinant
		DirectX::XMVECTOR normal = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
		DirectX::XMVECTOR direction = DirectX::XMVector3Normalize(DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&apex), DirectX::XMLoadFloat3(&right[rightIter])));
		float side = DirectX::XMVector3Dot(DirectX::XMVector3Cross(DirectX::XMVectorSubtract(DirectX::XMLoadFloat3(&left[leftIter]), DirectX::XMLoadFloat3(&apex)), direction), normal).m128_f32[0];

		//if side < 0, then edge[0] is on the left
		if (side < 0.0f) {
			overlap = false;
		}
		//If side > 0, then edge[0] is on the right
		else {
			overlap = true;
		}

		//No overlap
		if (!overlap) {

			//Angle is larger
			if (newAngle > angle) {

				angle = newAngle;

				if (movedLeft) {
					leftIter--;
					rightIter++;
					movedLeft = false;
				}
				else {
					rightIter--;
					leftIter++;
					movedLeft = true;
				}
			}
			//Angle is smaller
			else {
				angle = newAngle;

				if (movedLeft) {
					rightIter++;
					movedLeft = false;
				}
				else {
					leftIter++;
					movedLeft = true;
				}
			}
		}
		//Overlap
		else {
			if (movedLeft) {
				if (!apexArr.empty()) {
					if ((int)apexArr[apexArr.size() - 1].x == (int)right[rightIter].x &&
						(int)apexArr[apexArr.size() - 1].y == (int)right[rightIter].y &&
						(int)apexArr[apexArr.size() - 1].z == (int)right[rightIter].z) {
						rightIter++;
						leftIter = rightIter;
						angle = FLT_MAX;
						continue;
					}
					else {
						apexArr.push_back(right[rightIter]);
					}
				}
				else {
					apexArr.push_back(right[rightIter]);
				}

				apex = right[rightIter];
				rightIter++;
				leftIter = rightIter;
				angle = FLT_MAX;
			}
			else {
				if (!apexArr.empty()) {
					if ((int)apexArr[apexArr.size() - 1].x == (int)left[leftIter].x &&
						(int)apexArr[apexArr.size() - 1].y == (int)left[leftIter].y &&
						(int)apexArr[apexArr.size() - 1].z == (int)left[leftIter].z) {
						leftIter++;
						rightIter = leftIter;
						angle = FLT_MAX;
						continue;
					}
					else {
						apexArr.push_back(left[leftIter]);
					}
				}
				else {
					apexArr.push_back(left[leftIter]);
				}
				apex = left[leftIter];
				leftIter++;
				rightIter = leftIter;
				angle = FLT_MAX;
			}
		}
	}

	////////////////////////////////////////////////
	//Shouldnt need this
	//F I X   I T !!!!!!
	if (apexArr[apexArr.size() - 1].x != goal.x ||
		apexArr[apexArr.size() - 1].y != goal.y ||
		apexArr[apexArr.size() - 1].z != goal.z) {
		apexArr.push_back(goal);
	}
	////////////////////////////////////////////////

	unsigned int curApex = 0;
	unsigned int curSide = 0;

	//Loop through apex array
	while (curApex + 1 < apexArr.size()) {
		//Set current 2D apex line (Looking down Y)
		DirectX::XMFLOAT2 s1(apexArr[curApex].x, apexArr[curApex].z);
		DirectX::XMFLOAT2 e1(apexArr[curApex + 1].x, apexArr[curApex + 1].z);
		bool crosses = true;

		//Loop through left and right verts until lines are no longer touching
		//or there are no more verts left in array
		while (crosses && curSide < left.size()) {
			//If the current left or right vert is equal to the end of the apex line
			if (apexArr[curApex + 1].x == left[curSide].x && apexArr[curApex + 1].y == left[curSide].y && apexArr[curApex + 1].z == left[curSide].z ||
				apexArr[curApex + 1].x == right[curSide].x && apexArr[curApex + 1].y == right[curSide].y && apexArr[curApex + 1].z == right[curSide].z) {
				//If path is not empty
				if (!path.empty()) {
					//If end of apex line is not already in the path list, add it
					if (path[path.size() - 1].x == apexArr[curApex + 1].x &&
						path[path.size() - 1].y == apexArr[curApex + 1].y &&
						path[path.size() - 1].z == apexArr[curApex + 1].z) {
						curSide++;
						continue;
					}
					else {
						path.push_back(apexArr[curApex + 1]);
					}
				}
				//Path is empty so add the apex to path
				else {
					path.push_back(apexArr[curApex + 1]);
				}
			}
			//The current left or right vert is not equal to the end of the apex line
			else {
				DirectX::XMFLOAT2 interPoint;
				DirectX::XMFLOAT2 s2(left[curSide].x, left[curSide].z);
				DirectX::XMFLOAT2 e2(right[curSide].x, right[curSide].z);

				//Check if the 2D apex line and the 2D left/right vert line crosses
				crosses = DoLinesIntersect2D(s1, e1, s2, e2, interPoint);

				//If 2D lines cross
				if (crosses) {
					float y;

					//Find the Y component on a 3D line at X and Z
					if (FindY(left[curSide], right[curSide], interPoint.x, interPoint.y, y)) {
						path.push_back(DirectX::XMFLOAT3(interPoint.x, y, interPoint.y));
					}
				}
			}

			curSide++;
		}
		curApex++;
	}

	path.push_back(goal);

	return path;
}