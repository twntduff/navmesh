#include "Input.h"

Input::Input(){
}

Input::~Input(){
	//Shutdown();
}

POINT Input::GetMousePos() {
	return mMousePosition; 
}

const float Input::GetMouseDx() const{
	return (float)mMouseState.lX; 
}

const float Input::GetMouseDy() const{ 
	return (float)mMouseState.lY;
}

const float Input::GetMouseDz() const{
	return (float)mMouseState.lZ;
}

bool Input::Initialize(HINSTANCE hInst, HWND hWnd){
	HRESULT result;

	ZeroMemory(mKeyBuffer, sizeof(mKeyBuffer));
	ZeroMemory(&mMouseState, sizeof(mMouseState));
	ZeroMemory(mKeyBuffer, sizeof(mKeyBuffer));
	ZeroMemory(mLastKeyBuffer, sizeof(mLastKeyBuffer));
	ZeroMemory(mCurrKeyBuffer, sizeof(mCurrKeyBuffer));

	result = DirectInput8Create(hInst, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&mDInput, 0);
	if (FAILED(result)) {
		return false;
	}

	result = mDInput->CreateDevice(GUID_SysKeyboard, &mKeyboard, 0);
	if (FAILED(result)) {
		return false;
	}

	result = mKeyboard->SetDataFormat(&c_dfDIKeyboard);
	if (FAILED(result)) {
		return false;
	}

	result = mKeyboard->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	if (FAILED(result)) {
		return false;
	}

	result = mKeyboard->Acquire();
	if (FAILED(result)) {
		return false;
	}

	result = mDInput->CreateDevice(GUID_SysMouse, &mMouse, 0);
	if (FAILED(result)) {
		return false;
	}

	result = mMouse->SetDataFormat(&c_dfDIMouse2);
	if (FAILED(result)) {
		return false;
	}

	result = mMouse->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_FOREGROUND);
	if (FAILED(result)) {
		return false;
	}

	result = mMouse->Acquire();
	if (FAILED(result)) {
		return false;
	}

	return true;
}

void Input::Shutdown() {
	if (mDInput) {
		mDInput->Release();
		delete mDInput;
		mDInput = 0;
	}

	if (mKeyboard) {
		mKeyboard->Unacquire();
		delete mKeyboard;
		mKeyboard = 0;
	}

	if (mMouse) {
		mMouse->Unacquire();
		delete mMouse;
		mMouse = 0;
	}

	if (mKeyboard) {
		mKeyboard->Release();
		delete mKeyboard;
		mKeyboard = 0;
	}

	if (mMouse) {
		mMouse->Release();
		delete mMouse;
		mMouse = 0;
	}
}

void Input::CheckDevice(HWND hWnd)
{
	HRESULT result = mKeyboard->GetDeviceState(sizeof(mKeyBuffer), (void**)&mKeyBuffer);
	if (FAILED(result))
	{
		//Keyboard lost
		ZeroMemory(mKeyBuffer, sizeof(mKeyBuffer));

		result = mKeyboard->Acquire();
	}

	result = mMouse->GetDeviceState(sizeof(DIMOUSESTATE2), (void**)&mMouseState);
	if (FAILED(result))
	{
		//Mouse lost
		ZeroMemory(&mMouseState, sizeof(mMouseState));

		result = mMouse->Acquire();
	}

	POINT mouse;
	GetCursorPos(&mouse);
	ScreenToClient(hWnd, &mouse);

	mMousePosition.x = mouse.x;
	mMousePosition.y = mouse.y;
}

KeyState Input::GetKeyState(int key)
{
	KeyState temp;

	mCurrKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	if ((mCurrKeyBuffer[key] != mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 0))
		temp = Pressed;
	else if ((mCurrKeyBuffer[key] == mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 1))
		temp = Held;
	else if ((mCurrKeyBuffer[key] != mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 1))
		temp = Released;
	else
		temp = NoChange;

	mLastKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	return temp;
}

bool Input::IsKeyPressed(int key)
{
	bool temp;

	mCurrKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	temp = (mCurrKeyBuffer[key] != mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 0);

	mLastKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	return temp;
}

bool Input::IsKeyHeld(int key)
{
	bool temp;

	mCurrKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	temp = (mCurrKeyBuffer[key] == mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 1);

	mLastKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	return temp;
}

bool Input::IsKeyReleased(int key)
{
	bool temp = false;

	mCurrKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	if (mCurrKeyBuffer[key] != mLastKeyBuffer[key])
	{
		temp = (mCurrKeyBuffer[key] != mLastKeyBuffer[key]) && (mLastKeyBuffer[key] == 1);
	}
	mLastKeyBuffer[key] = ((mKeyBuffer[key] & 0x80) != 0);

	return temp;
}

//void Input::KeyDown(unsigned int input){
//	// If a key is pressed then save that state in the key array.
//	mKeys[input] = true;
//	return;
//}
//
//
//void Input::KeyUp(unsigned int input){
//	// If a key is released then clear that state in the key array.
//	mKeys[input] = false;
//	return;
//}
//
//
//bool Input::IsKeyDown(unsigned int key){
//	// Return what state the key is in (pressed//not pressed).
//	return mKeys[key];
//}
