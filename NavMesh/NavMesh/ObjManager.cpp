#include "ObjManager.h"
#include "eSceneComp.h"
#include "eVehicle.h"
#include "CollisionDetector.h"

ObjManager::ObjManager(){
}

ObjManager::~ObjManager(){

}

bool ObjManager::Initialize(ID3D11Device* device) {
	mRenderer.reset(new Renderer);
	if (!mRenderer->Initialize(device)) {
		return false;
	}

	return true;
}

unsigned int ObjManager::GetNextValidID() {
	if (mObjects.empty()) {
		return 0;
	}
	
	return (unsigned int)mObjects.size();
}

eObject* ObjManager::AddObject(eObject* const object) {
	if (!object) {
		return nullptr;
	}

	std::unique_ptr<eObject> obj(object);
	obj->mID = GetNextValidID();

	obj.swap(mObjects[obj->mID]);

	return object;
}

eObject* ObjManager::AddSceneComp(Mesh* const mesh, const DirectX::XMFLOAT3 position,
	const DirectX::XMFLOAT3 scale, const DirectX::XMFLOAT3 pitchYawRoll) {
	eSceneComp* sceneComp = new eSceneComp;

	sceneComp->Initialize(mesh, scale, position, pitchYawRoll);

	return AddObject(sceneComp);
}

eObject* ObjManager::AddNavMesh(Mesh* const mesh, const DirectX::XMFLOAT3 position,
	const DirectX::XMFLOAT3 scale, const DirectX::XMFLOAT3 pitchYawRoll) {
	eNavMesh* navMesh = new eNavMesh;

	navMesh->Initialize(mesh, scale, position, pitchYawRoll);

	return AddObject(navMesh);
}

eObject* ObjManager::AddVehicle(Mesh* const mesh, const DirectX::XMFLOAT3 position,
	const DirectX::XMFLOAT3 scale, const DirectX::XMFLOAT3 pitchYawRoll, const float maxVelocity) {
	eVehicle* vehicle = new eVehicle;

	vehicle->Initialize(mesh, scale, position, pitchYawRoll, maxVelocity);

	return AddObject(vehicle);
}

bool ObjManager::DeleteAllObjects() {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator iter = mObjects.begin();

	while (iter != mObjects.end()) {
		iter = mObjects.erase(iter);
	}

	if (!mObjects.empty()) {
		return false;
	}

	return true;
}

bool ObjManager::DeleteObject(unsigned int id) {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator iter = mObjects.find(id);

	if (iter->second) {
		mObjects.erase(iter);
		return true;
	}

	return false;
}

void ObjManager::UpdateObjects(const float dt) {
	std::unordered_map<unsigned int, std::unique_ptr<eObject>>::const_iterator iter = mObjects.begin();
	
	while (iter != mObjects.end()) {
		if (iter->second) {
			iter->second->Frame(dt);
		}
		iter++;
	}
}

void ObjManager::Render(Direct3D* d3d, const DirectX::XMFLOAT4X4 view) {
	mRenderer->RenderScene(&mObjects, d3d, view);
}

void ObjManager::ScreenPicked(POINT mousePos, DirectX::XMFLOAT3 rayOrigin, DirectX::XMFLOAT4X4 view, DirectX::XMFLOAT4X4 projection, unsigned int screenWidth, unsigned int screenHeight) {
	DirectX::XMVECTOR mouse3D = DirectX::XMVector3Unproject(DirectX::XMVectorSet((float)mousePos.x, (float)mousePos.y, 0.0f, 1.0f), 0.0f, 0.0f, (float)screenWidth, (float)screenHeight, 0.0f, 1.0f, DirectX::XMLoadFloat4x4(&projection), DirectX::XMLoadFloat4x4(&view), DirectX::XMMatrixIdentity());
	DirectX::XMVECTOR dir = DirectX::XMVector3Normalize(DirectX::XMVectorSubtract(mouse3D, DirectX::XMLoadFloat3(&rayOrigin)));

	DirectX::XMFLOAT3 origin;
	DirectX::XMStoreFloat3(&origin, mouse3D);
	DirectX::XMFLOAT3 direction;
	DirectX::XMStoreFloat3(&direction, dir);

	DirectX::XMFLOAT3 interPoint;
	if (CollisionDetector::RayVsNavMeshIntersect(&mObjects, origin, direction, interPoint)) {
		for (unsigned int i = 0; i < mObjects.size(); i++) {
			auto navMesh = dynamic_cast<eNavMesh*>(mObjects[i].get());

			if (navMesh) {
				for (unsigned int j = 0; j < mObjects.size(); j++) {
					auto vehicle = dynamic_cast<eVehicle*>(mObjects[j].get());

					if (vehicle) {
						vehicle->mPath = navMesh->FindPath(vehicle->mPosition, interPoint);
						break;
					}
				}
				break;
			}
		}
	}
}