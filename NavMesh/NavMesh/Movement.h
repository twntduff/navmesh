#ifndef _MOVEMENT_H_
#define _MOVEMENT_H_

//Includes
#include <d3d11.h>
#include <DirectXMath.h>

class Movement
{
public:
	Movement();
	~Movement();

	static DirectX::XMFLOAT3 Seek(const float maxSpeed, const DirectX::XMFLOAT3 target, const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 velocity);
};

#endif