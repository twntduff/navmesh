#ifndef _DIRECT3D_H_
#define _DIRECT3D_H_

//Linking
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "DirectXTK.lib")

//Includes
#include <atlbase.h>
#include <d3d11.h>
#include <DirectXMath.h>

class Direct3D
{
public:
	bool mVsyncEnabled;
	int mVideoCardMemory;
	char mVideoCardDescription[128];
	ATL::CComPtr<IDXGISwapChain> mSwapChain;
	ATL::CComPtr<ID3D11Device> mDevice;
	ATL::CComPtr<ID3D11DeviceContext> mDeviceContext;
	ATL::CComPtr<ID3D11RenderTargetView> mRenderTargetView;
	ATL::CComPtr<ID3D11Texture2D> mDepthStencilBuffer;
	ATL::CComPtr<ID3D11DepthStencilState> mDepthStencilState;
	ATL::CComPtr<ID3D11DepthStencilState> mDepthDisabledStencilState;
	ATL::CComPtr<ID3D11DepthStencilView> mDepthStencilView;
	ATL::CComPtr<ID3D11RasterizerState> mRasterState;
	D3D11_VIEWPORT mViewport;
	DirectX::XMFLOAT4X4 mProjectionMatrix;
	DirectX::XMFLOAT4X4 mOrthographicMatrix;

public:
	Direct3D();
	~Direct3D();

	bool Initialize(int, int, bool, HWND, bool, float, float);
	void Shutdown();

	void BeginScene(float, float, float, float);
	void EndScene();
};

#endif