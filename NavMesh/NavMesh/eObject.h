#ifndef _EOBJECT_H_
#define _EOBJECT_H_

#include <DirectXMath.h>
#include <d3d11.h>

class eObject
{
public:
	DirectX::XMFLOAT4X4 mTransform;
	DirectX::XMFLOAT3 mPosition;
	DirectX::XMFLOAT4 mOrientation;
	unsigned int mID;

public:
	eObject();
	virtual ~eObject();

	bool Initialize(const DirectX::XMFLOAT3 position, const DirectX::XMFLOAT3 pitchYawRoll);
	virtual void Frame(const float dt);

	DirectX::XMFLOAT3 GetAxis(const unsigned int axis);
};

#endif