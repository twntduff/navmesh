#include "ColorShader.h"

ColorShader::ColorShader(){
}

ColorShader::~ColorShader(){
}

bool ColorShader::Initialize(ID3D11Device* device) {
	ID3DBlob *vertexShaderBuffer;
	ID3DBlob *pixelShaderBuffer;
	HRESULT hresult;

#if defined(DEBUG) || defined(_DEBUG)  
	ID3DBlob *errorMessage;

	hresult = D3DCompileFromFile(L"ColorVertex.hlsl", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "VS_MAIN", "vs_5_0", D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG, 0, &vertexShaderBuffer, &errorMessage);
	if (FAILED(hresult)) {
		return false;
	}
	hresult = D3DCompileFromFile(L"ColorPixel.hlsl", NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, "PS_MAIN", "ps_5_0", D3DCOMPILE_ENABLE_STRICTNESS | D3DCOMPILE_DEBUG, 0, &pixelShaderBuffer, &errorMessage);
	if (FAILED(hresult)) {
		return false;
	}

#else

	D3DReadFileToBlob(L"ColorVertex.cso", &vertexShaderBuffer);
	D3DReadFileToBlob(L"ColorPixel.cso", &pixelShaderBuffer);

#endif

	hresult = device->CreateVertexShader(vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), NULL, &mVertexShader);
	if (FAILED(hresult)) {
		return false;
	}
	hresult = device->CreatePixelShader(pixelShaderBuffer->GetBufferPointer(), pixelShaderBuffer->GetBufferSize(), NULL, &mPixelShader);
	if (FAILED(hresult)) {
		return false;
	}

	D3D11_INPUT_ELEMENT_DESC polygonLayout[2];
	polygonLayout[0].SemanticName = "POSITION";
	polygonLayout[0].SemanticIndex = 0;
	polygonLayout[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
	polygonLayout[0].InputSlot = 0;
	polygonLayout[0].AlignedByteOffset = 0;
	polygonLayout[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[0].InstanceDataStepRate = 0;

	polygonLayout[1].SemanticName = "COLOR";
	polygonLayout[1].SemanticIndex = 0;
	polygonLayout[1].Format = DXGI_FORMAT_R32G32B32A32_FLOAT;
	polygonLayout[1].InputSlot = 0;
	polygonLayout[1].AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
	polygonLayout[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
	polygonLayout[1].InstanceDataStepRate = 0;

	unsigned int numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

	hresult = device->CreateInputLayout(polygonLayout, numElements, vertexShaderBuffer->GetBufferPointer(), vertexShaderBuffer->GetBufferSize(), &mInputLayout);
	if (FAILED(hresult)) {
		return false;
	}

	vertexShaderBuffer->Release();
	vertexShaderBuffer = 0;

	pixelShaderBuffer->Release();
	pixelShaderBuffer = 0;

	D3D11_BUFFER_DESC matrixBufferDesc;
	matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	matrixBufferDesc.ByteWidth = sizeof(MatrixBuffer);
	matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	matrixBufferDesc.MiscFlags = 0;
	matrixBufferDesc.StructureByteStride = 0;

	hresult = device->CreateBuffer(&matrixBufferDesc, NULL, &mMatrixBuffer);
	if (FAILED(hresult)) {
		return false;
	}

	return true;
}

void ColorShader::Render(ID3D11DeviceContext* deviceContext, const unsigned int indexCount, const DirectX::XMFLOAT4X4 world, const DirectX::XMFLOAT4X4 view, const DirectX::XMFLOAT4X4 projection) {
	HRESULT hresult;
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	MatrixBuffer* dataPtr;

	//Set shader parameters
	DirectX::XMFLOAT4X4 xmWorld;
	DirectX::XMFLOAT4X4 xmView;
	DirectX::XMFLOAT4X4 xmProjection;

	DirectX::XMStoreFloat4x4(&xmWorld, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&world)));
	DirectX::XMStoreFloat4x4(&xmView, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&view)));
	DirectX::XMStoreFloat4x4(&xmProjection, DirectX::XMMatrixTranspose(DirectX::XMLoadFloat4x4(&projection)));

	hresult = deviceContext->Map(mMatrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	if (FAILED(hresult)) {
		return;
	}

	dataPtr = (MatrixBuffer*)mappedResource.pData;
	dataPtr->World = xmWorld;
	dataPtr->View = xmView;
	dataPtr->Projection = xmProjection;

	deviceContext->Unmap(mMatrixBuffer, 0);

	deviceContext->VSSetConstantBuffers(0, 1, &mMatrixBuffer.p);

	//Render shader
	deviceContext->IASetInputLayout(mInputLayout);

	deviceContext->VSSetShader(mVertexShader, NULL, 0);
	deviceContext->PSSetShader(mPixelShader, NULL, 0);

	deviceContext->DrawIndexed(indexCount, 0, 0);
}